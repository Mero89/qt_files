# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/Users/mero/Projects/SKope/skope/ui/qtproject/TitreForm/dat.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_DatScreen(object):
    def setupUi(self, DatScreen):
        DatScreen.setObjectName(_fromUtf8("DatScreen"))
        DatScreen.resize(348, 366)
        self.verticalLayout = QtGui.QVBoxLayout(DatScreen)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.horizontalLayout_7 = QtGui.QHBoxLayout()
        self.horizontalLayout_7.setObjectName(_fromUtf8("horizontalLayout_7"))
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_7.addItem(spacerItem)
        self.lbl_title = QtGui.QLabel(DatScreen)
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(9, 91, 118))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(9, 91, 118))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(9, 91, 118))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.ButtonText, brush)
        brush = QtGui.QBrush(QtGui.QColor(9, 91, 118))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(9, 91, 118))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(9, 91, 118))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.ButtonText, brush)
        brush = QtGui.QBrush(QtGui.QColor(127, 127, 127))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(127, 127, 127))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(148, 148, 148))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.ButtonText, brush)
        self.lbl_title.setPalette(palette)
        font = QtGui.QFont()
        font.setPointSize(16)
        font.setBold(True)
        font.setWeight(75)
        self.lbl_title.setFont(font)
        self.lbl_title.setTextFormat(QtCore.Qt.RichText)
        self.lbl_title.setScaledContents(True)
        self.lbl_title.setIndent(3)
        self.lbl_title.setTextInteractionFlags(QtCore.Qt.TextSelectableByMouse)
        self.lbl_title.setObjectName(_fromUtf8("lbl_title"))
        self.horizontalLayout_7.addWidget(self.lbl_title)
        spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_7.addItem(spacerItem1)
        self.verticalLayout.addLayout(self.horizontalLayout_7)
        self.line_3 = QtGui.QFrame(DatScreen)
        self.line_3.setFrameShape(QtGui.QFrame.HLine)
        self.line_3.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_3.setObjectName(_fromUtf8("line_3"))
        self.verticalLayout.addWidget(self.line_3)
        self.formLayout = QtGui.QFormLayout()
        self.formLayout.setLabelAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.formLayout.setObjectName(_fromUtf8("formLayout"))
        self.typeLabel = QtGui.QLabel(DatScreen)
        self.typeLabel.setObjectName(_fromUtf8("typeLabel"))
        self.formLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.typeLabel)
        self.cb_type = QtGui.QComboBox(DatScreen)
        self.cb_type.setObjectName(_fromUtf8("cb_type"))
        self.formLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.cb_type)
        self.nominalLabel = QtGui.QLabel(DatScreen)
        self.nominalLabel.setObjectName(_fromUtf8("nominalLabel"))
        self.formLayout.setWidget(1, QtGui.QFormLayout.LabelRole, self.nominalLabel)
        self.txt_nominal = QtGui.QLineEdit(DatScreen)
        self.txt_nominal.setObjectName(_fromUtf8("txt_nominal"))
        self.formLayout.setWidget(1, QtGui.QFormLayout.FieldRole, self.txt_nominal)
        self.tauxFacialLabel = QtGui.QLabel(DatScreen)
        self.tauxFacialLabel.setObjectName(_fromUtf8("tauxFacialLabel"))
        self.formLayout.setWidget(2, QtGui.QFormLayout.LabelRole, self.tauxFacialLabel)
        self.txt_taux_facial = QtGui.QLineEdit(DatScreen)
        self.txt_taux_facial.setObjectName(_fromUtf8("txt_taux_facial"))
        self.formLayout.setWidget(2, QtGui.QFormLayout.FieldRole, self.txt_taux_facial)
        self.emissionLabel = QtGui.QLabel(DatScreen)
        self.emissionLabel.setObjectName(_fromUtf8("emissionLabel"))
        self.formLayout.setWidget(3, QtGui.QFormLayout.LabelRole, self.emissionLabel)
        self.dt_emission = QtGui.QDateEdit(DatScreen)
        self.dt_emission.setDate(QtCore.QDate(2016, 1, 1))
        self.dt_emission.setObjectName(_fromUtf8("dt_emission"))
        self.formLayout.setWidget(3, QtGui.QFormLayout.FieldRole, self.dt_emission)
        self.echeanceLabel = QtGui.QLabel(DatScreen)
        self.echeanceLabel.setObjectName(_fromUtf8("echeanceLabel"))
        self.formLayout.setWidget(4, QtGui.QFormLayout.LabelRole, self.echeanceLabel)
        self.dt_echeance = QtGui.QDateEdit(DatScreen)
        self.dt_echeance.setDate(QtCore.QDate(2016, 1, 1))
        self.dt_echeance.setObjectName(_fromUtf8("dt_echeance"))
        self.formLayout.setWidget(4, QtGui.QFormLayout.FieldRole, self.dt_echeance)
        self.choisirLeFondsLabel = QtGui.QLabel(DatScreen)
        self.choisirLeFondsLabel.setObjectName(_fromUtf8("choisirLeFondsLabel"))
        self.formLayout.setWidget(6, QtGui.QFormLayout.LabelRole, self.choisirLeFondsLabel)
        self.cb_fonds = QtGui.QComboBox(DatScreen)
        self.cb_fonds.setMaxVisibleItems(15)
        self.cb_fonds.setSizeAdjustPolicy(QtGui.QComboBox.AdjustToContents)
        self.cb_fonds.setObjectName(_fromUtf8("cb_fonds"))
        self.formLayout.setWidget(6, QtGui.QFormLayout.FieldRole, self.cb_fonds)
        self.emetteurLabel = QtGui.QLabel(DatScreen)
        self.emetteurLabel.setObjectName(_fromUtf8("emetteurLabel"))
        self.formLayout.setWidget(5, QtGui.QFormLayout.LabelRole, self.emetteurLabel)
        self.cb_emetteur = QtGui.QComboBox(DatScreen)
        self.cb_emetteur.setMaxVisibleItems(15)
        self.cb_emetteur.setSizeAdjustPolicy(QtGui.QComboBox.AdjustToContents)
        self.cb_emetteur.setObjectName(_fromUtf8("cb_emetteur"))
        self.formLayout.setWidget(5, QtGui.QFormLayout.FieldRole, self.cb_emetteur)
        self.verticalLayout.addLayout(self.formLayout)
        self.horizontalLayout_5 = QtGui.QHBoxLayout()
        self.horizontalLayout_5.setObjectName(_fromUtf8("horizontalLayout_5"))
        spacerItem2 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_5.addItem(spacerItem2)
        self.bu_save = QtGui.QPushButton(DatScreen)
        self.bu_save.setMinimumSize(QtCore.QSize(150, 0))
        self.bu_save.setObjectName(_fromUtf8("bu_save"))
        self.horizontalLayout_5.addWidget(self.bu_save)
        spacerItem3 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_5.addItem(spacerItem3)
        self.verticalLayout.addLayout(self.horizontalLayout_5)
        spacerItem4 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem4)

        self.retranslateUi(DatScreen)
        QtCore.QMetaObject.connectSlotsByName(DatScreen)
        DatScreen.setTabOrder(self.cb_type, self.txt_nominal)
        DatScreen.setTabOrder(self.txt_nominal, self.txt_taux_facial)
        DatScreen.setTabOrder(self.txt_taux_facial, self.dt_emission)
        DatScreen.setTabOrder(self.dt_emission, self.dt_echeance)
        DatScreen.setTabOrder(self.dt_echeance, self.cb_emetteur)
        DatScreen.setTabOrder(self.cb_emetteur, self.cb_fonds)
        DatScreen.setTabOrder(self.cb_fonds, self.bu_save)

    def retranslateUi(self, DatScreen):
        DatScreen.setWindowTitle(_translate("DatScreen", "Form", None))
        self.lbl_title.setText(_translate("DatScreen", "Dépôt à terme", None))
        self.typeLabel.setText(_translate("DatScreen", "Type", None))
        self.nominalLabel.setText(_translate("DatScreen", "Nominal", None))
        self.tauxFacialLabel.setText(_translate("DatScreen", "Taux facial", None))
        self.emissionLabel.setText(_translate("DatScreen", "Emission", None))
        self.dt_emission.setDisplayFormat(_translate("DatScreen", "dd/MM/yyyy", None))
        self.echeanceLabel.setText(_translate("DatScreen", "Echeance", None))
        self.dt_echeance.setDisplayFormat(_translate("DatScreen", "dd/MM/yyyy", None))
        self.choisirLeFondsLabel.setText(_translate("DatScreen", "Choisir le Fonds", None))
        self.emetteurLabel.setText(_translate("DatScreen", "Emetteur", None))
        self.bu_save.setText(_translate("DatScreen", "Enregistrer", None))

