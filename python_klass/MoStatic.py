# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/Users/mero/Projects/PricerMO/DPricer/ui/qtproject/TitreForm/mo.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8


    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)


class Ui_MO(object):
    def setupUi(self, MO):
        MO.setObjectName(_fromUtf8("MO"))
        MO.resize(568, 383)
        self.verticalLayout_8 = QtGui.QVBoxLayout(MO)
        self.verticalLayout_8.setObjectName(_fromUtf8("verticalLayout_8"))
        self.tabWidget = QtGui.QTabWidget(MO)
        self.tabWidget.setTabShape(QtGui.QTabWidget.Rounded)
        self.tabWidget.setDocumentMode(True)
        self.tabWidget.setObjectName(_fromUtf8("tabWidget"))
        self.tab_manar = QtGui.QWidget()
        self.tab_manar.setObjectName(_fromUtf8("tab_manar"))
        self.verticalLayout = QtGui.QVBoxLayout(self.tab_manar)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.groupBox_9 = QtGui.QGroupBox(self.tab_manar)
        font = QtGui.QFont()
        font.setBold(False)
        font.setWeight(50)
        self.groupBox_9.setFont(font)
        self.groupBox_9.setFlat(False)
        self.groupBox_9.setObjectName(_fromUtf8("groupBox_9"))
        self.verticalLayout_13 = QtGui.QVBoxLayout(self.groupBox_9)
        self.verticalLayout_13.setObjectName(_fromUtf8("verticalLayout_13"))
        self.horizontalLayout_6 = QtGui.QHBoxLayout()
        self.horizontalLayout_6.setObjectName(_fromUtf8("horizontalLayout_6"))
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_6.addItem(spacerItem)
        self.label_4 = QtGui.QLabel(self.groupBox_9)
        font = QtGui.QFont()
        font.setBold(False)
        font.setWeight(50)
        self.label_4.setFont(font)
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.horizontalLayout_6.addWidget(self.label_4)
        self.dt_query = QtGui.QDateEdit(self.groupBox_9)
        font = QtGui.QFont()
        self.dt_query.setFont(font)
        self.dt_query.setFocusPolicy(QtCore.Qt.TabFocus)
        self.dt_query.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.dt_query.setButtonSymbols(QtGui.QAbstractSpinBox.PlusMinus)
        self.dt_query.setCalendarPopup(True)
        self.dt_query.setDate(QtCore.QDate(2015, 1, 15))
        self.dt_query.setObjectName(_fromUtf8("dt_query"))
        self.horizontalLayout_6.addWidget(self.dt_query)
        spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_6.addItem(spacerItem1)
        self.verticalLayout_13.addLayout(self.horizontalLayout_6)
        self.horizontalLayout_7 = QtGui.QHBoxLayout()
        self.horizontalLayout_7.setObjectName(_fromUtf8("horizontalLayout_7"))
        spacerItem2 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_7.addItem(spacerItem2)
        self.label_5 = QtGui.QLabel(self.groupBox_9)
        font = QtGui.QFont()
        font.setBold(False)
        font.setWeight(50)
        self.label_5.setFont(font)
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.horizontalLayout_7.addWidget(self.label_5)
        self.cb_context = QtGui.QComboBox(self.groupBox_9)
        self.cb_context.setMaxCount(100)
        self.cb_context.setSizeAdjustPolicy(QtGui.QComboBox.AdjustToContents)
        self.cb_context.setObjectName(_fromUtf8("cb_context"))
        self.horizontalLayout_7.addWidget(self.cb_context)
        spacerItem3 = QtGui.QSpacerItem(25, 20, QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_7.addItem(spacerItem3)
        self.bu_generer_requete = QtGui.QPushButton(self.groupBox_9)
        font = QtGui.QFont()
        font.setWeight(50)
        self.bu_generer_requete.setFont(font)
        self.bu_generer_requete.setObjectName(_fromUtf8("bu_generer_requete"))
        self.horizontalLayout_7.addWidget(self.bu_generer_requete)
        self.verticalLayout_13.addLayout(self.horizontalLayout_7)
        self.verticalLayout.addWidget(self.groupBox_9)
        self.groupBox_3 = QtGui.QGroupBox(self.tab_manar)
        self.groupBox_3.setFlat(True)
        self.groupBox_3.setObjectName(_fromUtf8("groupBox_3"))
        self.verticalLayout_6 = QtGui.QVBoxLayout(self.groupBox_3)
        self.verticalLayout_6.setContentsMargins(6, 12, 6, 0)
        self.verticalLayout_6.setObjectName(_fromUtf8("verticalLayout_6"))
        self.text_holder = QtGui.QTextEdit(self.groupBox_3)
        self.text_holder.setFrameShape(QtGui.QFrame.Box)
        self.text_holder.setReadOnly(True)
        self.text_holder.setObjectName(_fromUtf8("text_holder"))
        self.verticalLayout_6.addWidget(self.text_holder)
        self.verticalLayout.addWidget(self.groupBox_3)
        self.tabWidget.addTab(self.tab_manar, _fromUtf8(""))
        self.tab_journal = QtGui.QWidget()
        self.tab_journal.setObjectName(_fromUtf8("tab_journal"))
        self.verticalLayout_5 = QtGui.QVBoxLayout(self.tab_journal)
        self.verticalLayout_5.setObjectName(_fromUtf8("verticalLayout_5"))
        self.horizontalLayout_3 = QtGui.QHBoxLayout()
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.verticalLayout_4 = QtGui.QVBoxLayout()
        self.verticalLayout_4.setObjectName(_fromUtf8("verticalLayout_4"))
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setSizeConstraint(QtGui.QLayout.SetDefaultConstraint)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.lineEdit = QtGui.QLineEdit(self.tab_journal)
        self.lineEdit.setMinimumSize(QtCore.QSize(160, 0))
        self.lineEdit.setInputMask(_fromUtf8(""))
        self.lineEdit.setObjectName(_fromUtf8("lineEdit"))
        self.horizontalLayout.addWidget(self.lineEdit)
        self.bu_choose_file = QtGui.QPushButton(self.tab_journal)
        self.bu_choose_file.setMaximumSize(QtCore.QSize(110, 16777215))
        self.bu_choose_file.setObjectName(_fromUtf8("bu_choose_file"))
        self.horizontalLayout.addWidget(self.bu_choose_file)
        self.verticalLayout_4.addLayout(self.horizontalLayout)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.label = QtGui.QLabel(self.tab_journal)
        self.label.setScaledContents(True)
        self.label.setObjectName(_fromUtf8("label"))
        self.horizontalLayout_2.addWidget(self.label)
        self.dt_valo = QtGui.QDateEdit(self.tab_journal)
        self.dt_valo.setCalendarPopup(True)
        self.dt_valo.setDate(QtCore.QDate(2015, 9, 18))
        self.dt_valo.setObjectName(_fromUtf8("dt_valo"))
        self.horizontalLayout_2.addWidget(self.dt_valo)
        self.verticalLayout_4.addLayout(self.horizontalLayout_2)
        self.horizontalLayout_3.addLayout(self.verticalLayout_4)
        self.line = QtGui.QFrame(self.tab_journal)
        self.line.setFrameShape(QtGui.QFrame.VLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName(_fromUtf8("line"))
        self.horizontalLayout_3.addWidget(self.line)
        self.grp_radio = QtGui.QGroupBox(self.tab_journal)
        self.grp_radio.setObjectName(_fromUtf8("grp_radio"))
        self.verticalLayout_3 = QtGui.QVBoxLayout(self.grp_radio)
        self.verticalLayout_3.setObjectName(_fromUtf8("verticalLayout_3"))
        self.rd_bleu_nuit = QtGui.QRadioButton(self.grp_radio)
        self.rd_bleu_nuit.setChecked(True)
        self.rd_bleu_nuit.setObjectName(_fromUtf8("rd_bleu_nuit"))
        self.verticalLayout_3.addWidget(self.rd_bleu_nuit)
        self.rd_bleu = QtGui.QRadioButton(self.grp_radio)
        self.rd_bleu.setChecked(False)
        self.rd_bleu.setObjectName(_fromUtf8("rd_bleu"))
        self.verticalLayout_3.addWidget(self.rd_bleu)
        self.horizontalLayout_3.addWidget(self.grp_radio)
        self.verticalLayout_5.addLayout(self.horizontalLayout_3)
        self.horizontalLayout_4 = QtGui.QHBoxLayout()
        self.horizontalLayout_4.setSizeConstraint(QtGui.QLayout.SetDefaultConstraint)
        self.horizontalLayout_4.setContentsMargins(-1, 0, -1, -1)
        self.horizontalLayout_4.setObjectName(_fromUtf8("horizontalLayout_4"))
        spacerItem4 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_4.addItem(spacerItem4)
        self.bu_journal_valo = QtGui.QPushButton(self.tab_journal)
        self.bu_journal_valo.setMinimumSize(QtCore.QSize(130, 0))
        self.bu_journal_valo.setObjectName(_fromUtf8("bu_journal_valo"))
        self.horizontalLayout_4.addWidget(self.bu_journal_valo)
        spacerItem5 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_4.addItem(spacerItem5)
        self.verticalLayout_5.addLayout(self.horizontalLayout_4)
        spacerItem6 = QtGui.QSpacerItem(20, 319, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_5.addItem(spacerItem6)
        self.tabWidget.addTab(self.tab_journal, _fromUtf8(""))
        self.tab_courbe = QtGui.QWidget()
        self.tab_courbe.setObjectName(_fromUtf8("tab_courbe"))
        self.verticalLayout_7 = QtGui.QVBoxLayout(self.tab_courbe)
        self.verticalLayout_7.setObjectName(_fromUtf8("verticalLayout_7"))
        self.horizontalLayout_5 = QtGui.QHBoxLayout()
        self.horizontalLayout_5.setObjectName(_fromUtf8("horizontalLayout_5"))
        self.label_2 = QtGui.QLabel(self.tab_courbe)
        self.label_2.setScaledContents(True)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.horizontalLayout_5.addWidget(self.label_2)
        self.dt_courbe = QtGui.QDateEdit(self.tab_courbe)
        self.dt_courbe.setCalendarPopup(True)
        self.dt_courbe.setDate(QtCore.QDate(2015, 9, 18))
        self.dt_courbe.setObjectName(_fromUtf8("dt_courbe"))
        self.horizontalLayout_5.addWidget(self.dt_courbe)
        spacerItem7 = QtGui.QSpacerItem(154, 20, QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_5.addItem(spacerItem7)
        self.verticalLayout_7.addLayout(self.horizontalLayout_5)
        self.groupBox = QtGui.QGroupBox(self.tab_courbe)
        self.groupBox.setObjectName(_fromUtf8("groupBox"))
        self.horizontalLayout_8 = QtGui.QHBoxLayout(self.groupBox)
        self.horizontalLayout_8.setObjectName(_fromUtf8("horizontalLayout_8"))
        self.rd_manar = QtGui.QRadioButton(self.groupBox)
        self.rd_manar.setChecked(True)
        self.rd_manar.setObjectName(_fromUtf8("rd_manar"))
        self.horizontalLayout_8.addWidget(self.rd_manar)
        self.rd_zero_coupon = QtGui.QRadioButton(self.groupBox)
        self.rd_zero_coupon.setObjectName(_fromUtf8("rd_zero_coupon"))
        self.horizontalLayout_8.addWidget(self.rd_zero_coupon)
        self.rd_bam = QtGui.QRadioButton(self.groupBox)
        self.rd_bam.setObjectName(_fromUtf8("rd_bam"))
        self.horizontalLayout_8.addWidget(self.rd_bam)
        self.rd_bam_brute = QtGui.QRadioButton(self.groupBox)
        self.rd_bam_brute.setObjectName(_fromUtf8("rd_bam_brute"))
        self.horizontalLayout_8.addWidget(self.rd_bam_brute)
        self.verticalLayout_7.addWidget(self.groupBox)
        self.horizontalLayout_9 = QtGui.QHBoxLayout()
        self.horizontalLayout_9.setSizeConstraint(QtGui.QLayout.SetDefaultConstraint)
        self.horizontalLayout_9.setContentsMargins(-1, 0, -1, -1)
        self.horizontalLayout_9.setObjectName(_fromUtf8("horizontalLayout_9"))
        spacerItem8 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_9.addItem(spacerItem8)
        self.bu_courbe_fichier = QtGui.QPushButton(self.tab_courbe)
        self.bu_courbe_fichier.setMinimumSize(QtCore.QSize(130, 0))
        self.bu_courbe_fichier.setObjectName(_fromUtf8("bu_courbe_fichier"))
        self.horizontalLayout_9.addWidget(self.bu_courbe_fichier)
        spacerItem9 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_9.addItem(spacerItem9)
        self.verticalLayout_7.addLayout(self.horizontalLayout_9)
        spacerItem10 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_7.addItem(spacerItem10)
        self.tabWidget.addTab(self.tab_courbe, _fromUtf8(""))
        self.tab_actions = QtGui.QWidget()
        self.tab_actions.setObjectName(_fromUtf8("tab_actions"))
        self.verticalLayout_2 = QtGui.QVBoxLayout(self.tab_actions)
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.horizontalLayout_14 = QtGui.QHBoxLayout()
        self.horizontalLayout_14.setObjectName(_fromUtf8("horizontalLayout_14"))
        self.label_3 = QtGui.QLabel(self.tab_actions)
        self.label_3.setScaledContents(True)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.horizontalLayout_14.addWidget(self.label_3)
        self.dt_cotation = QtGui.QDateEdit(self.tab_actions)
        self.dt_cotation.setCalendarPopup(True)
        self.dt_cotation.setDate(QtCore.QDate(2015, 9, 18))
        self.dt_cotation.setObjectName(_fromUtf8("dt_cotation"))
        self.horizontalLayout_14.addWidget(self.dt_cotation)
        spacerItem11 = QtGui.QSpacerItem(154, 20, QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_14.addItem(spacerItem11)
        self.verticalLayout_2.addLayout(self.horizontalLayout_14)
        self.horizontalLayout_10 = QtGui.QHBoxLayout()
        self.horizontalLayout_10.setSizeConstraint(QtGui.QLayout.SetDefaultConstraint)
        self.horizontalLayout_10.setObjectName(_fromUtf8("horizontalLayout_10"))
        self.lineEdit_file_src = QtGui.QLineEdit(self.tab_actions)
        self.lineEdit_file_src.setMinimumSize(QtCore.QSize(160, 0))
        self.lineEdit_file_src.setInputMask(_fromUtf8(""))
        self.lineEdit_file_src.setObjectName(_fromUtf8("lineEdit_file_src"))
        self.horizontalLayout_10.addWidget(self.lineEdit_file_src)
        self.bu_choose_file_src = QtGui.QPushButton(self.tab_actions)
        self.bu_choose_file_src.setMaximumSize(QtCore.QSize(110, 16777215))
        self.bu_choose_file_src.setObjectName(_fromUtf8("bu_choose_file_src"))
        self.horizontalLayout_10.addWidget(self.bu_choose_file_src)
        self.verticalLayout_2.addLayout(self.horizontalLayout_10)
        self.horizontalLayout_11 = QtGui.QHBoxLayout()
        self.horizontalLayout_11.setSizeConstraint(QtGui.QLayout.SetDefaultConstraint)
        self.horizontalLayout_11.setObjectName(_fromUtf8("horizontalLayout_11"))
        self.lineEdit_file_target = QtGui.QLineEdit(self.tab_actions)
        self.lineEdit_file_target.setMinimumSize(QtCore.QSize(160, 0))
        self.lineEdit_file_target.setInputMask(_fromUtf8(""))
        self.lineEdit_file_target.setObjectName(_fromUtf8("lineEdit_file_target"))
        self.horizontalLayout_11.addWidget(self.lineEdit_file_target)
        self.bu_choose_file_target = QtGui.QPushButton(self.tab_actions)
        self.bu_choose_file_target.setMaximumSize(QtCore.QSize(110, 16777215))
        self.bu_choose_file_target.setObjectName(_fromUtf8("bu_choose_file_target"))
        self.horizontalLayout_11.addWidget(self.bu_choose_file_target)
        self.verticalLayout_2.addLayout(self.horizontalLayout_11)
        self.groupBox_2 = QtGui.QGroupBox(self.tab_actions)
        self.groupBox_2.setObjectName(_fromUtf8("groupBox_2"))
        self.horizontalLayout_12 = QtGui.QHBoxLayout(self.groupBox_2)
        self.horizontalLayout_12.setObjectName(_fromUtf8("horizontalLayout_12"))
        self.rd_sicovam = QtGui.QRadioButton(self.groupBox_2)
        self.rd_sicovam.setChecked(True)
        self.rd_sicovam.setObjectName(_fromUtf8("rd_sicovam"))
        self.horizontalLayout_12.addWidget(self.rd_sicovam)
        self.rd_ponderation = QtGui.QRadioButton(self.groupBox_2)
        self.rd_ponderation.setObjectName(_fromUtf8("rd_ponderation"))
        self.horizontalLayout_12.addWidget(self.rd_ponderation)
        self.verticalLayout_2.addWidget(self.groupBox_2)
        self.horizontalLayout_13 = QtGui.QHBoxLayout()
        self.horizontalLayout_13.setObjectName(_fromUtf8("horizontalLayout_13"))
        spacerItem12 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_13.addItem(spacerItem12)
        self.bu_convert_file = QtGui.QPushButton(self.tab_actions)
        self.bu_convert_file.setObjectName(_fromUtf8("bu_convert_file"))
        self.horizontalLayout_13.addWidget(self.bu_convert_file)
        spacerItem13 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_13.addItem(spacerItem13)
        self.verticalLayout_2.addLayout(self.horizontalLayout_13)
        spacerItem14 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_2.addItem(spacerItem14)
        self.tabWidget.addTab(self.tab_actions, _fromUtf8(""))
        self.verticalLayout_8.addWidget(self.tabWidget)

        self.retranslateUi(MO)
        self.tabWidget.setCurrentIndex(0)
        self.cb_context.setCurrentIndex(-1)
        QtCore.QMetaObject.connectSlotsByName(MO)

    def retranslateUi(self, MO):
        MO.setWindowTitle(_translate("MO", "MO", None))
        self.groupBox_9.setTitle(_translate("MO", "Control Box", None))
        self.label_4.setText(_translate("MO", "Sélectionner date :  ", None))
        self.dt_query.setToolTip(_translate("MO",
                                            "<html><head/><body><p>La date choisie sera intégrée à la requête générée.</p></body></html>",
                                            None))
        self.dt_query.setDisplayFormat(_translate("MO", "dd/MM/yy", None))
        self.label_5.setText(_translate("MO", "Choisir le contexte : ", None))
        self.cb_context.setToolTip(_translate("MO",
                                              "<html><head/><body><p>Choisir le contexte de la requete :</p><p>  • Requete des titres récemment crées sur MANAR.</p><p>  • Requete de la Valorisation Manar.</p><p>  • Requete de l\'echeancier du titre sur MANAR.</p></body></html>",
                                              None))
        self.bu_generer_requete.setText(_translate("MO", "Générer Requête", None))
        self.groupBox_3.setTitle(_translate("MO", "Copier - Coller votre requête", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_manar), _translate("MO", "Manar", None))
        self.lineEdit.setPlaceholderText(_translate("MO", "Fichier de Valorisation MANAR", None))
        self.bu_choose_file.setText(_translate("MO", "Choisir fichier", None))
        self.label.setText(_translate("MO", "Date Valeur :", None))
        self.dt_valo.setDisplayFormat(_translate("MO", "d/M/yyyy", None))
        self.grp_radio.setTitle(_translate("MO", "Couleur entête", None))
        self.rd_bleu_nuit.setText(_translate("MO", "Bleu nuit", None))
        self.rd_bleu.setText(_translate("MO", "Bleu", None))
        self.bu_journal_valo.setText(_translate("MO", "Générer Journal de valorisation", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_journal),
                                  _translate("MO", "Journal Valorisation", None))
        self.label_2.setText(_translate("MO", "Date Valeur :", None))
        self.dt_courbe.setDisplayFormat(_translate("MO", "d/M/yyyy", None))
        self.groupBox.setTitle(_translate("MO", "Mode courbe", None))
        self.rd_manar.setText(_translate("MO", "Manar format (CSV)", None))
        self.rd_zero_coupon.setText(_translate("MO", "Zéro Coupon", None))
        self.rd_bam.setText(_translate("MO", "BAM", None))
        self.rd_bam_brute.setText(_translate("MO", "BAM Brute", None))
        self.bu_courbe_fichier.setText(_translate("MO", "Générer fichier", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_courbe), _translate("MO", "Courbe", None))
        self.label_3.setText(_translate("MO", "Date Cotation :", None))
        self.dt_cotation.setDisplayFormat(_translate("MO", "d/M/yyyy", None))
        self.lineEdit_file_src.setPlaceholderText(_translate("MO", "chemin du fichier Sicovam source", None))
        self.bu_choose_file_src.setText(_translate("MO", "Choisir fichier", None))
        self.lineEdit_file_target.setPlaceholderText(_translate("MO", "chemin du fichier cible", None))
        self.bu_choose_file_target.setText(_translate("MO", "Choisir fichier", None))
        self.groupBox_2.setTitle(_translate("MO", "Type du fichier source", None))
        self.rd_sicovam.setText(_translate("MO", "Sicovam (SogéBourse)", None))
        self.rd_ponderation.setText(_translate("MO", "Pondération (Site de la bourse)", None))
        self.bu_convert_file.setText(_translate("MO", "Convertir Fichier", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_actions), _translate("MO", "Actions", None))
