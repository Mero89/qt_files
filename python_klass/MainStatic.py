# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/Users/mero/Projects/PricerMO/DPricer/ui/qtproject/TitreForm/main.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

from PyQt4.QtCore import Qt
from PyQt4.QtGui import *
import os


class MDIArea(QMdiArea):

    def __init__(self, background_pixmap, parent=None):
    
        QMdiArea.__init__(self, parent)
        self.background_pixmap = background_pixmap
        self.centered = False
    
    def paintEvent(self, event):
    
        painter = QPainter()
        painter.begin(self.viewport())
        
        if not self.centered:
            painter.drawPixmap(0, 0, self.width(), self.height(), self.background_pixmap)
        else:
            painter.fillRect(event.rect(), self.palette().color(QPalette.Window))
            x = (self.width() - self.display_pixmap.width())/2
            y = (self.height() - self.display_pixmap.height())/2
            painter.drawPixmap(x, y, self.display_pixmap)
        
        painter.end()
    
    def resizeEvent(self, event):
    
        self.display_pixmap = self.background_pixmap.scaled(event.size(), Qt.KeepAspectRatio)

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)


class Ui_MainWindow(object):

    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(680, 452)
        MainWindow.setDockNestingEnabled(True)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.verticalLayout = QtGui.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        here = os.path.dirname(os.path.abspath(__file__))
        pixpath = os.path.join(here, 'background.png')
        self.mdiArea = MDIArea(QPixmap(pixpath), self.centralwidget)
        self.mdiArea.setObjectName(_fromUtf8("mdiArea"))
        self.verticalLayout.addWidget(self.mdiArea)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 680, 22))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        self.menuAbout = QtGui.QMenu(self.menubar)
        self.menuAbout.setObjectName(_fromUtf8("menuAbout"))
        self.menuDonnees = QtGui.QMenu(self.menubar)
        self.menuDonnees.setObjectName(_fromUtf8("menuDonnees"))
        self.menuBase = QtGui.QMenu(self.menubar)
        self.menuBase.setObjectName(_fromUtf8("menuBase"))
        self.menuBenchmarks = QtGui.QMenu(self.menubar)
        self.menuBenchmarks.setObjectName(_fromUtf8("menuBenchmarks"))
        self.menuSpecifique = QtGui.QMenu(self.menubar)
        self.menuSpecifique.setObjectName(_fromUtf8("menuSpecifique"))
        self.menuRapports = QtGui.QMenu(self.menubar)
        self.menuRapports.setObjectName(_fromUtf8("menuRapports"))
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        MainWindow.setStatusBar(self.statusbar)
        self.actionDat = QtGui.QAction(MainWindow)
        self.actionDat.setObjectName(_fromUtf8("actionDat"))
        self.actionPrepareDb = QtGui.QAction(MainWindow)
        self.actionPrepareDb.setObjectName(_fromUtf8("actionPrepareDb"))
        self.actionImporter = QtGui.QAction(MainWindow)
        self.actionImporter.setObjectName(_fromUtf8("actionImporter"))
        self.actionExporter = QtGui.QAction(MainWindow)
        self.actionExporter.setObjectName(_fromUtf8("actionExporter"))
        self.actionTemplate = QtGui.QAction(MainWindow)
        self.actionTemplate.setObjectName(_fromUtf8("actionTemplate"))
        self.actionMarketTemplate = QtGui.QAction(MainWindow)
        self.actionMarketTemplate.setObjectName(_fromUtf8("actionMarketTemplate"))
        self.actionAdmin = QtGui.QAction(MainWindow)
        self.actionAdmin.setObjectName(_fromUtf8("actionAdmin"))
        self.actionEcheanciers = QtGui.QAction(MainWindow)
        self.actionEcheanciers.setObjectName(_fromUtf8("actionEcheanciers"))
        self.actionValorisations = QtGui.QAction(MainWindow)
        self.actionValorisations.setObjectName(_fromUtf8("actionValorisations"))
        self.actionBenchmarks = QtGui.QAction(MainWindow)
        self.actionBenchmarks.setObjectName(_fromUtf8("actionBenchmarks"))
        self.actionDPricer = QtGui.QAction(MainWindow)
        self.actionDPricer.setObjectName(_fromUtf8("actionDPricer"))
        self.action_importer_market = QtGui.QAction(MainWindow)
        self.action_importer_market.setObjectName(_fromUtf8("action_importer_market"))
        self.action_exporter_market = QtGui.QAction(MainWindow)
        self.action_exporter_market.setObjectName(_fromUtf8("action_exporter_market"))
        self.actionMarch = QtGui.QAction(MainWindow)
        self.actionMarch.setObjectName(_fromUtf8("actionMarch"))
        self.actionReportData = QtGui.QAction(MainWindow)
        self.actionReportData.setObjectName(_fromUtf8("actionBenchmarks"))
        self.actionRapports = QtGui.QAction(MainWindow)
        self.actionRapports.setObjectName(_fromUtf8("actionRapports"))
        self.menuAbout.addAction(self.actionDPricer)
        self.menuRapports.addAction(self.actionRapports)
        self.menuSpecifique.addAction(self.actionDat)
        self.menuSpecifique.addSeparator()
        self.menuSpecifique.addAction(self.actionPrepareDb)
        self.menuSpecifique.addAction(self.actionReportData)
        self.menuDonnees.addAction(self.actionImporter)
        self.menuDonnees.addAction(self.actionExporter)
        self.menuDonnees.addSeparator()
        self.menuDonnees.addAction(self.actionTemplate)
        self.menuBase.addAction(self.actionAdmin)
        self.menuBase.addAction(self.actionMarch)
        self.menuBenchmarks.addAction(self.actionBenchmarks)
        self.menuBenchmarks.addSeparator()
        self.menuBenchmarks.addAction(self.action_importer_market)
        self.menuBenchmarks.addAction(self.action_exporter_market)
        self.menuBenchmarks.addSeparator()
        self.menuBenchmarks.addAction(self.actionMarketTemplate)
        self.menubar.addAction(self.menuBase.menuAction())
        self.menubar.addAction(self.menuDonnees.menuAction())
        self.menubar.addAction(self.menuBenchmarks.menuAction())
        self.menubar.addAction(self.menuSpecifique.menuAction())
        self.menubar.addAction(self.menuRapports.menuAction())
        self.menubar.addAction(self.menuAbout.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "Reporting data producer", None))
        self.menuAbout.setTitle(_translate("MainWindow", "About", None))
        self.menuDonnees.setTitle(_translate("MainWindow", "SKG", None))
        self.menuBase.setTitle(_translate("MainWindow", "Base", None))
        self.menuSpecifique.setTitle(_translate("MainWindow", "Spécifique", None))
        self.menuRapports.setTitle(_translate("MainWindow", "Rapports", None))
        self.menuBenchmarks.setTitle(_translate("MainWindow", "Market", None))
        self.actionImporter.setText(_translate("MainWindow", "Importer", None))
        self.actionExporter.setText(_translate("MainWindow", "Exporter", None))
        self.actionDat.setText(_translate("MainWindow", "Créer un DAT", None))
        self.actionPrepareDb.setText(_translate("MainWindow", "Préparer la Base de données", None))
        self.actionTemplate.setText(_translate("MainWindow", "Generer Template", None))
        self.actionMarketTemplate.setText(_translate("MainWindow", "Generer Market Template", None))
        self.actionAdmin.setText(_translate("MainWindow", "SKG", None))
        self.actionEcheanciers.setText(_translate("MainWindow", "Echeanciers", None))
        self.actionValorisations.setText(_translate("MainWindow", "Valorisations", None))
        self.actionBenchmarks.setText(_translate("MainWindow", "Benchmarks", None))
        self.actionDPricer.setText(_translate("MainWindow", "Skope", None))
        self.action_importer_market.setText(_translate("MainWindow", "Importer", None))
        self.action_exporter_market.setText(_translate("MainWindow", "exporter", None))
        self.actionMarch.setText(_translate("MainWindow", "Marché", None))
        self.actionReportData.setText(_translate("MainWindow", "Générer Reporting", None))

