# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/Users/mero/Projects/PricerMO/DPricer/ui/qtproject/TitreForm/valo.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8


    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)


class Ui_Valo_Screen(object):
    
    def setupUi(self, Valo_Screen):
        Valo_Screen.setObjectName(_fromUtf8("Valo_Screen"))
        Valo_Screen.resize(400, 170)
        Valo_Screen.setMaximumSize(QtCore.QSize(400, 170))
        self.verticalLayout = QtGui.QVBoxLayout(Valo_Screen)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.horizontalLayout_6 = QtGui.QHBoxLayout()
        self.horizontalLayout_6.setObjectName(_fromUtf8("horizontalLayout_6"))
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_6.addItem(spacerItem)
        self.label_4 = QtGui.QLabel(Valo_Screen)
        font = QtGui.QFont()
        font.setBold(False)
        font.setWeight(50)
        self.label_4.setFont(font)
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.horizontalLayout_6.addWidget(self.label_4)
        self.dt_valo = QtGui.QDateEdit(Valo_Screen)
        self.dt_valo.setFocusPolicy(QtCore.Qt.TabFocus)
        self.dt_valo.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.dt_valo.setButtonSymbols(QtGui.QAbstractSpinBox.PlusMinus)
        self.dt_valo.setCalendarPopup(True)
        self.dt_valo.setDate(QtCore.QDate(2015, 1, 15))
        self.dt_valo.setObjectName(_fromUtf8("dt_valo"))
        self.horizontalLayout_6.addWidget(self.dt_valo)
        spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_6.addItem(spacerItem1)
        self.verticalLayout.addLayout(self.horizontalLayout_6)
        self.horizontalLayout_7 = QtGui.QHBoxLayout()
        self.horizontalLayout_7.setObjectName(_fromUtf8("horizontalLayout_7"))
        spacerItem2 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_7.addItem(spacerItem2)
        self.bu_valoriser_titres = QtGui.QPushButton(Valo_Screen)
        self.bu_valoriser_titres.setObjectName(_fromUtf8("bu_valoriser_titres"))
        self.horizontalLayout_7.addWidget(self.bu_valoriser_titres)
        self.bu_supprimer_valo_titres = QtGui.QPushButton(Valo_Screen)
        self.bu_supprimer_valo_titres.setObjectName(_fromUtf8("bu_supprimer_valo_titres"))
        self.horizontalLayout_7.addWidget(self.bu_supprimer_valo_titres)
        spacerItem3 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_7.addItem(spacerItem3)
        self.verticalLayout.addLayout(self.horizontalLayout_7)
        spacerItem4 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem4)

        self.retranslateUi(Valo_Screen)
        QtCore.QMetaObject.connectSlotsByName(Valo_Screen)

    def retranslateUi(self, Valo_Screen):
        Valo_Screen.setWindowTitle(_translate("Valo_Screen", "Valoriser Les titres", None))
        self.label_4.setText(_translate("Valo_Screen", "Date Valorisation :  ", None))
        self.dt_valo.setToolTip(
            _translate("Valo_Screen", "<html><head/><body><p>Choisir la Date de Valorisation.</p></body></html>", None))
        self.dt_valo.setDisplayFormat(_translate("Valo_Screen", "dd/MM/yy", None))
        self.bu_valoriser_titres.setText(_translate("Valo_Screen", "Valoriser Titres", None))
        self.bu_supprimer_valo_titres.setText(_translate("Valo_Screen", "Supprimer Valo", None))
