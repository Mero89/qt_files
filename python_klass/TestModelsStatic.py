# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'qtproject/TitreForm/testmodels.ui'
#
# Created: Wed Nov 11 15:57:39 2015
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_TestModelsScreen(object):
    def setupUi(self, TestModelsScreen):
        TestModelsScreen.setObjectName(_fromUtf8("TestModelsScreen"))
        TestModelsScreen.resize(589, 312)
        self.verticalLayout = QtGui.QVBoxLayout(TestModelsScreen)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.horizontalLayout_6 = QtGui.QHBoxLayout()
        self.horizontalLayout_6.setObjectName(_fromUtf8("horizontalLayout_6"))
        self.label_3 = QtGui.QLabel(TestModelsScreen)
        self.label_3.setScaledContents(True)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.horizontalLayout_6.addWidget(self.label_3)
        self.cb_fields = QtGui.QComboBox(TestModelsScreen)
        self.cb_fields.setMaxCount(200)
        self.cb_fields.setInsertPolicy(QtGui.QComboBox.InsertAlphabetically)
        self.cb_fields.setObjectName(_fromUtf8("cb_fields"))
        self.horizontalLayout_6.addWidget(self.cb_fields)
        self.cb_operator = QtGui.QComboBox(TestModelsScreen)
        self.cb_operator.setMaximumSize(QtCore.QSize(55, 16777215))
        self.cb_operator.setMaxCount(10)
        self.cb_operator.setInsertPolicy(QtGui.QComboBox.InsertAtBottom)
        self.cb_operator.setObjectName(_fromUtf8("cb_operator"))
        self.cb_operator.addItem(_fromUtf8(""))
        self.cb_operator.addItem(_fromUtf8(""))
        self.cb_operator.addItem(_fromUtf8(""))
        self.cb_operator.addItem(_fromUtf8(""))
        self.cb_operator.addItem(_fromUtf8(""))
        self.horizontalLayout_6.addWidget(self.cb_operator)
        self.txt_value = QtGui.QLineEdit(TestModelsScreen)
        self.txt_value.setMinimumSize(QtCore.QSize(90, 0))
        self.txt_value.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.txt_value.setObjectName(_fromUtf8("txt_value"))
        self.horizontalLayout_6.addWidget(self.txt_value)
        self.bu_filter = QtGui.QPushButton(TestModelsScreen)
        self.bu_filter.setObjectName(_fromUtf8("bu_filter"))
        self.horizontalLayout_6.addWidget(self.bu_filter)
        spacerItem = QtGui.QSpacerItem(154, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_6.addItem(spacerItem)
        self.verticalLayout.addLayout(self.horizontalLayout_6)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.tableView = QtGui.QTableView(TestModelsScreen)
        self.tableView.setAlternatingRowColors(True)
        self.tableView.setObjectName(_fromUtf8("tableView"))
        self.tableView.horizontalHeader().setStretchLastSection(True)
        self.horizontalLayout_2.addWidget(self.tableView)
        self.verticalLayout_2 = QtGui.QVBoxLayout()
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.bu_add_model = QtGui.QPushButton(TestModelsScreen)
        self.bu_add_model.setObjectName(_fromUtf8("bu_add_model"))
        self.verticalLayout_2.addWidget(self.bu_add_model)
        self.bu_save_changes = QtGui.QPushButton(TestModelsScreen)
        self.bu_save_changes.setObjectName(_fromUtf8("bu_save_changes"))
        self.verticalLayout_2.addWidget(self.bu_save_changes)
        self.bu_delete_model = QtGui.QPushButton(TestModelsScreen)
        self.bu_delete_model.setObjectName(_fromUtf8("bu_delete_model"))
        self.verticalLayout_2.addWidget(self.bu_delete_model)
        spacerItem1 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_2.addItem(spacerItem1)
        self.horizontalLayout_2.addLayout(self.verticalLayout_2)
        self.verticalLayout.addLayout(self.horizontalLayout_2)

        self.retranslateUi(TestModelsScreen)
        self.cb_operator.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(TestModelsScreen)

    def retranslateUi(self, TestModelsScreen):
        TestModelsScreen.setWindowTitle(_translate("TestModelsScreen", "Form", None))
        self.label_3.setText(_translate("TestModelsScreen", "Filtrer par champs :", None))
        self.cb_operator.setItemText(0, _translate("TestModelsScreen", "=", None))
        self.cb_operator.setItemText(1, _translate("TestModelsScreen", "<=", None))
        self.cb_operator.setItemText(2, _translate("TestModelsScreen", ">=", None))
        self.cb_operator.setItemText(3, _translate("TestModelsScreen", "<", None))
        self.cb_operator.setItemText(4, _translate("TestModelsScreen", ">", None))
        self.txt_value.setPlaceholderText(_translate("TestModelsScreen", "Saisir valeur", None))
        self.bu_filter.setText(_translate("TestModelsScreen", "Filtrer", None))
        self.bu_add_model.setText(_translate("TestModelsScreen", "Ajouter", None))
        self.bu_save_changes.setText(_translate("TestModelsScreen", "Enregistrer", None))
        self.bu_delete_model.setText(_translate("TestModelsScreen", "Supprimer", None))

