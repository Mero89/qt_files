# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/Users/mero/Projects/PricerMO/DPricer/ui/qtproject/TitreForm/database.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_DBAdmin(object):
    def setupUi(self, DBAdmin):
        DBAdmin.setObjectName(_fromUtf8("DBAdmin"))
        DBAdmin.resize(626, 378)
        self.verticalLayout = QtGui.QVBoxLayout(DBAdmin)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.horizontalLayout_5 = QtGui.QHBoxLayout()
        self.horizontalLayout_5.setObjectName(_fromUtf8("horizontalLayout_5"))
        self.label_2 = QtGui.QLabel(DBAdmin)
        self.label_2.setScaledContents(True)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.horizontalLayout_5.addWidget(self.label_2)
        self.cb_tables = QtGui.QComboBox(DBAdmin)
        self.cb_tables.setMaxCount(200)
        self.cb_tables.setInsertPolicy(QtGui.QComboBox.InsertAlphabetically)
        self.cb_tables.setObjectName(_fromUtf8("cb_tables"))
        self.horizontalLayout_5.addWidget(self.cb_tables)
        spacerItem = QtGui.QSpacerItem(184, 20, QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_5.addItem(spacerItem)
        self.verticalLayout.addLayout(self.horizontalLayout_5)
        self.horizontalLayout_6 = QtGui.QHBoxLayout()
        self.horizontalLayout_6.setObjectName(_fromUtf8("horizontalLayout_6"))
        self.label_3 = QtGui.QLabel(DBAdmin)
        self.label_3.setScaledContents(True)
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.horizontalLayout_6.addWidget(self.label_3)
        self.cb_fields = QtGui.QComboBox(DBAdmin)
        self.cb_fields.setMaxCount(200)
        self.cb_fields.setInsertPolicy(QtGui.QComboBox.InsertAlphabetically)
        self.cb_fields.setObjectName(_fromUtf8("cb_fields"))
        self.horizontalLayout_6.addWidget(self.cb_fields)
        self.cb_operator = QtGui.QComboBox(DBAdmin)
        self.cb_operator.setMaximumSize(QtCore.QSize(55, 16777215))
        self.cb_operator.setMaxCount(10)
        self.cb_operator.setInsertPolicy(QtGui.QComboBox.InsertAtBottom)
        self.cb_operator.setObjectName(_fromUtf8("cb_operator"))
        self.cb_operator.addItem(_fromUtf8(""))
        self.cb_operator.addItem(_fromUtf8(""))
        self.cb_operator.addItem(_fromUtf8(""))
        self.cb_operator.addItem(_fromUtf8(""))
        self.cb_operator.addItem(_fromUtf8(""))
        self.horizontalLayout_6.addWidget(self.cb_operator)
        self.lineEdit = QtGui.QLineEdit(DBAdmin)
        self.lineEdit.setMinimumSize(QtCore.QSize(90, 0))
        self.lineEdit.setCursorMoveStyle(QtCore.Qt.VisualMoveStyle)
        self.lineEdit.setObjectName(_fromUtf8("lineEdit"))
        self.horizontalLayout_6.addWidget(self.lineEdit)
        self.bu_filtrer = QtGui.QPushButton(DBAdmin)
        self.bu_filtrer.setObjectName(_fromUtf8("bu_filtrer"))
        self.horizontalLayout_6.addWidget(self.bu_filtrer)
        spacerItem1 = QtGui.QSpacerItem(154, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_6.addItem(spacerItem1)
        self.verticalLayout.addLayout(self.horizontalLayout_6)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.tableView = QtGui.QTableView(DBAdmin)
        self.tableView.setContextMenuPolicy(QtCore.Qt.ActionsContextMenu)
        self.tableView.setTabKeyNavigation(True)
        self.tableView.setDefaultDropAction(QtCore.Qt.CopyAction)
        self.tableView.setAlternatingRowColors(True)
        self.tableView.setSelectionMode(QtGui.QAbstractItemView.ContiguousSelection)
        self.tableView.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)
        self.tableView.setObjectName(_fromUtf8("tableView"))
        self.tableView.horizontalHeader().setStretchLastSection(True)
        self.horizontalLayout.addWidget(self.tableView)
        self.groupBox = QtGui.QGroupBox(DBAdmin)
        self.groupBox.setObjectName(_fromUtf8("groupBox"))
        self.verticalLayout_2 = QtGui.QVBoxLayout(self.groupBox)
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.bu_ajouter = QtGui.QPushButton(self.groupBox)
        self.bu_ajouter.setObjectName(_fromUtf8("bu_ajouter"))
        self.verticalLayout_2.addWidget(self.bu_ajouter)
        self.bu_supprimer = QtGui.QPushButton(self.groupBox)
        self.bu_supprimer.setObjectName(_fromUtf8("bu_supprimer"))
        self.verticalLayout_2.addWidget(self.bu_supprimer)
        self.bu_enregistrer = QtGui.QPushButton(self.groupBox)
        self.bu_enregistrer.setObjectName(_fromUtf8("bu_enregistrer"))
        self.verticalLayout_2.addWidget(self.bu_enregistrer)
        spacerItem2 = QtGui.QSpacerItem(20, 198, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout_2.addItem(spacerItem2)
        self.horizontalLayout.addWidget(self.groupBox)
        self.verticalLayout.addLayout(self.horizontalLayout)

        self.retranslateUi(DBAdmin)
        self.cb_operator.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(DBAdmin)
        DBAdmin.setTabOrder(self.cb_tables, self.cb_fields)
        DBAdmin.setTabOrder(self.cb_fields, self.cb_operator)
        DBAdmin.setTabOrder(self.cb_operator, self.lineEdit)
        DBAdmin.setTabOrder(self.lineEdit, self.bu_filtrer)
        DBAdmin.setTabOrder(self.bu_filtrer, self.bu_ajouter)
        DBAdmin.setTabOrder(self.bu_ajouter, self.bu_supprimer)
        DBAdmin.setTabOrder(self.bu_supprimer, self.tableView)

    def retranslateUi(self, DBAdmin):
        DBAdmin.setWindowTitle(_translate("DBAdmin", "Administration Base de données", None))
        self.label_2.setText(_translate("DBAdmin", "Choisir table :", None))
        self.label_3.setText(_translate("DBAdmin", "Filtrer par champs :", None))
        self.cb_operator.setItemText(0, _translate("DBAdmin", "=", None))
        self.cb_operator.setItemText(1, _translate("DBAdmin", "<=", None))
        self.cb_operator.setItemText(2, _translate("DBAdmin", ">=", None))
        self.cb_operator.setItemText(3, _translate("DBAdmin", "<", None))
        self.cb_operator.setItemText(4, _translate("DBAdmin", ">", None))
        self.lineEdit.setPlaceholderText(_translate("DBAdmin", "Saisir valeur", None))
        self.bu_filtrer.setText(_translate("DBAdmin", "Filtrer", None))
        self.groupBox.setTitle(_translate("DBAdmin", "Actions", None))
        self.bu_ajouter.setText(_translate("DBAdmin", "Ajouter", None))
        self.bu_supprimer.setText(_translate("DBAdmin", "Supprimer", None))
        self.bu_enregistrer.setText(_translate("DBAdmin", "Enregistrer", None))

