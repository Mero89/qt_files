# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'rapports.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)


class Ui_RapportsScreen(object):

    def setupUi(self, RapportsScreen):
        RapportsScreen.setObjectName(_fromUtf8("RapportsScreen"))
        RapportsScreen.resize(432, 355)
        self.verticalLayout = QtGui.QVBoxLayout(RapportsScreen)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.horizontalLayout_7 = QtGui.QHBoxLayout()
        self.horizontalLayout_7.setObjectName(_fromUtf8("horizontalLayout_7"))
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_7.addItem(spacerItem)
        self.lbl_title = QtGui.QLabel(RapportsScreen)
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(9, 91, 118))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(9, 91, 118))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(9, 91, 118))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.ButtonText, brush)
        brush = QtGui.QBrush(QtGui.QColor(9, 91, 118))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(9, 91, 118))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(9, 91, 118))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.ButtonText, brush)
        brush = QtGui.QBrush(QtGui.QColor(127, 127, 127))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.WindowText, brush)
        brush = QtGui.QBrush(QtGui.QColor(127, 127, 127))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(148, 148, 148))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.ButtonText, brush)
        self.lbl_title.setPalette(palette)
        font = QtGui.QFont()
        font.setPointSize(16)
        font.setBold(True)
        font.setWeight(75)
        self.lbl_title.setFont(font)
        self.lbl_title.setTextFormat(QtCore.Qt.RichText)
        self.lbl_title.setScaledContents(True)
        self.lbl_title.setIndent(3)
        self.lbl_title.setTextInteractionFlags(QtCore.Qt.TextSelectableByMouse)
        self.lbl_title.setObjectName(_fromUtf8("lbl_title"))
        self.horizontalLayout_7.addWidget(self.lbl_title)
        spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_7.addItem(spacerItem1)
        self.verticalLayout.addLayout(self.horizontalLayout_7)
        self.line_3 = QtGui.QFrame(RapportsScreen)
        self.line_3.setLineWidth(1)
        self.line_3.setFrameShape(QtGui.QFrame.HLine)
        self.line_3.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_3.setObjectName(_fromUtf8("line_3"))
        self.verticalLayout.addWidget(self.line_3)
        self.horizontalLayout_4 = QtGui.QHBoxLayout()
        self.horizontalLayout_4.setObjectName(_fromUtf8("horizontalLayout_4"))
        self.label_2 = QtGui.QLabel(RapportsScreen)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.horizontalLayout_4.addWidget(self.label_2)
        self.cb_choices = QtGui.QComboBox(RapportsScreen)
        self.cb_choices.setObjectName(_fromUtf8("cb_choices"))
        self.horizontalLayout_4.addWidget(self.cb_choices)
        self.verticalLayout.addLayout(self.horizontalLayout_4)
        self.line = QtGui.QFrame(RapportsScreen)
        self.line.setLineWidth(1)
        self.line.setFrameShape(QtGui.QFrame.HLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName(_fromUtf8("line"))
        self.verticalLayout.addWidget(self.line)
        self.groupBox = QtGui.QGroupBox(RapportsScreen)
        self.groupBox.setMinimumSize(QtCore.QSize(0, 50))
        self.groupBox.setFlat(False)
        self.groupBox.setObjectName(_fromUtf8("groupBox"))
        self.verticalLayout.addWidget(self.groupBox)
        spacerItem2 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem2)
        self.line_2 = QtGui.QFrame(RapportsScreen)
        self.line_2.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_2.setLineWidth(1)
        self.line_2.setFrameShape(QtGui.QFrame.HLine)
        self.line_2.setObjectName(_fromUtf8("line_2"))
        self.verticalLayout.addWidget(self.line_2)
        self.textBoard = QtGui.QTextBrowser(RapportsScreen)
        self.textBoard.setFrameShape(QtGui.QFrame.StyledPanel)
        self.textBoard.setFrameShadow(QtGui.QFrame.Sunken)
        self.textBoard.setObjectName(_fromUtf8("textBoard"))
        self.verticalLayout.addWidget(self.textBoard)

        self.retranslateUi(RapportsScreen)
        QtCore.QMetaObject.connectSlotsByName(RapportsScreen)

    def retranslateUi(self, RapportsScreen):
        RapportsScreen.setWindowTitle(_translate("RapportsScreen", "Rapports", None))
        self.lbl_title.setText(_translate("RapportsScreen", "Rapports SKOPE", None))
        self.label_2.setText(_translate("RapportsScreen", "Choisir le contexte du Rapport:", None))
        self.groupBox.setTitle(_translate("RapportsScreen", "Saisir les paramètres requis", None))

