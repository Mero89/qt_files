# -*- coding: latin-1 -*-

# Form implementation generated from reading ui file 'qtproject/TitreForm/export.ui'
#
# Created: Fri Oct 30 11:45:31 2015
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_ExportScreen(object):
    def setupUi(self, ExportScreen):
        ExportScreen.setObjectName(_fromUtf8("ExportScreen"))
        ExportScreen.resize(290, 140)
        ExportScreen.setMaximumSize(QtCore.QSize(320, 150))
        self.verticalLayout_2 = QtGui.QVBoxLayout(ExportScreen)
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.label_4 = QtGui.QLabel(ExportScreen)
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.horizontalLayout.addWidget(self.label_4)
        self.cb_export = QtGui.QComboBox(ExportScreen)
        self.cb_export.setInsertPolicy(QtGui.QComboBox.InsertAlphabetically)
        self.cb_export.setObjectName(_fromUtf8("cb_export"))
        self.cb_export.addItem(_fromUtf8(""))
        self.cb_export.addItem(_fromUtf8(""))
        self.cb_export.addItem(_fromUtf8(""))
        self.cb_export.addItem(_fromUtf8(""))
        self.cb_export.addItem(_fromUtf8(""))
        self.cb_export.addItem(_fromUtf8(""))
        self.cb_export.addItem(_fromUtf8(""))
        self.cb_export.addItem(_fromUtf8(""))
        self.cb_export.addItem(_fromUtf8(""))
        self.horizontalLayout.addWidget(self.cb_export)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.horizontalLayout_3 = QtGui.QHBoxLayout()
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.label = QtGui.QLabel(ExportScreen)
        self.label.setObjectName(_fromUtf8("label"))
        self.horizontalLayout_3.addWidget(self.label)
        self.dt_debut = QtGui.QDateEdit(ExportScreen)
        self.dt_debut.setButtonSymbols(QtGui.QAbstractSpinBox.UpDownArrows)
        self.dt_debut.setDate(QtCore.QDate(2015, 1, 1))
        self.dt_debut.setCalendarPopup(True)
        self.dt_debut.setObjectName(_fromUtf8("dt_debut"))
        self.horizontalLayout_3.addWidget(self.dt_debut)
        self.verticalLayout.addLayout(self.horizontalLayout_3)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.label_2 = QtGui.QLabel(ExportScreen)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.horizontalLayout_2.addWidget(self.label_2)
        self.dt_fin = QtGui.QDateEdit(ExportScreen)
        self.dt_fin.setButtonSymbols(QtGui.QAbstractSpinBox.UpDownArrows)
        self.dt_fin.setDate(QtCore.QDate(2015, 1, 1))
        self.dt_fin.setCalendarPopup(True)
        self.dt_fin.setObjectName(_fromUtf8("dt_fin"))
        self.horizontalLayout_2.addWidget(self.dt_fin)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.bu_export = QtGui.QPushButton(ExportScreen)
        self.bu_export.setObjectName(_fromUtf8("bu_export"))
        self.verticalLayout.addWidget(self.bu_export)
        self.verticalLayout_2.addLayout(self.verticalLayout)

        self.retranslateUi(ExportScreen)
        QtCore.QMetaObject.connectSlotsByName(ExportScreen)

    def retranslateUi(self, ExportScreen):
        ExportScreen.setWindowTitle(_translate("ExportScreen", "Form", None))
        self.label_4.setText(_translate("ExportScreen", "Choisir donn�es � exporter :", None))
        self.cb_export.setItemText(0, _translate("ExportScreen", "Courbe", None))
        self.cb_export.setItemText(1, _translate("ExportScreen", "Valorisation titres", None))
        self.cb_export.setItemText(2, _translate("ExportScreen", "MBI", None))
        self.cb_export.setItemText(3, _translate("ExportScreen", "MBI CT", None))
        self.cb_export.setItemText(4, _translate("ExportScreen", "MBI MT", None))
        self.cb_export.setItemText(5, _translate("ExportScreen", "MBI MLT", None))
        self.cb_export.setItemText(6, _translate("ExportScreen", "MBI LT", None))
        self.cb_export.setItemText(7, _translate("ExportScreen", "TMP", None))
        self.cb_export.setItemText(8, _translate("ExportScreen", "MASI-MADEX", None))
        self.label.setText(_translate("ExportScreen", "Date d�but :", None))
        self.label_2.setText(_translate("ExportScreen", "Date fin :", None))
        self.bu_export.setText(_translate("ExportScreen", "Exporter", None))

