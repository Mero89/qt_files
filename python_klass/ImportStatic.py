# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'import.ui'
#
# Created: Thu Sep 24 02:44:22 2015
#      by: PyQt4 UI code generator 4.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_ImportScreen(object):
    
    def setupUi(self, ImportScreen):
        ImportScreen.setObjectName(_fromUtf8("ImportScreen"))
        ImportScreen.resize(420, 250)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(ImportScreen.sizePolicy().hasHeightForWidth())
        ImportScreen.setSizePolicy(sizePolicy)
        ImportScreen.setMinimumSize(QtCore.QSize(350, 240))
        ImportScreen.setMaximumSize(QtCore.QSize(420, 250))
        self.verticalLayout = QtGui.QVBoxLayout(ImportScreen)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setSizeConstraint(QtGui.QLayout.SetDefaultConstraint)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.lineEdit = QtGui.QLineEdit(ImportScreen)
        self.lineEdit.setMinimumSize(QtCore.QSize(160, 0))
        self.lineEdit.setInputMask(_fromUtf8(""))
        self.lineEdit.setObjectName(_fromUtf8("lineEdit"))
        self.horizontalLayout.addWidget(self.lineEdit)
        self.bu_choose_file = QtGui.QPushButton(ImportScreen)
        self.bu_choose_file.setMaximumSize(QtCore.QSize(110, 16777215))
        self.bu_choose_file.setObjectName(_fromUtf8("bu_choose_file"))
        self.horizontalLayout.addWidget(self.bu_choose_file)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.line = QtGui.QFrame(ImportScreen)
        self.line.setFrameShape(QtGui.QFrame.HLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName(_fromUtf8("line"))
        self.verticalLayout.addWidget(self.line)
        self.groupBox = QtGui.QGroupBox(ImportScreen)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Preferred, QtGui.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.groupBox.sizePolicy().hasHeightForWidth())
        self.groupBox.setSizePolicy(sizePolicy)
        self.groupBox.setFlat(False)
        self.groupBox.setObjectName(_fromUtf8("groupBox"))
        self.gridLayout = QtGui.QGridLayout(self.groupBox)
        self.gridLayout.setObjectName(_fromUtf8("gridLayout"))
        self.rd_echeancier = QtGui.QRadioButton(self.groupBox)
        self.rd_echeancier.setObjectName(_fromUtf8("rd_echeancier"))
        self.gridLayout.addWidget(self.rd_echeancier, 2, 1, 1, 1)
        self.rd_titre = QtGui.QRadioButton(self.groupBox)
        self.rd_titre.setObjectName(_fromUtf8("rd_titre"))
        self.gridLayout.addWidget(self.rd_titre, 0, 1, 1, 1)
        self.rd_all_file = QtGui.QRadioButton(self.groupBox)
        self.rd_all_file.setObjectName(_fromUtf8("rd_all_file"))
        self.gridLayout.addWidget(self.rd_all_file, 4, 0, 1, 1)
        self.rd_courbe_front = QtGui.QRadioButton(self.groupBox)
        self.rd_courbe_front.setChecked(True)
        self.rd_courbe_front.setObjectName(_fromUtf8("rd_courbe_front"))
        self.gridLayout.addWidget(self.rd_courbe_front, 0, 0, 1, 1)
        self.rd_courbe = QtGui.QRadioButton(self.groupBox)
        self.rd_courbe.setObjectName(_fromUtf8("rd_courbe"))
        self.gridLayout.addWidget(self.rd_courbe, 2, 0, 1, 1)
        self.verticalLayout.addWidget(self.groupBox)
        self.line_2 = QtGui.QFrame(ImportScreen)
        self.line_2.setFrameShape(QtGui.QFrame.HLine)
        self.line_2.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_2.setObjectName(_fromUtf8("line_2"))
        self.verticalLayout.addWidget(self.line_2)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem)
        self.bu_import_file = QtGui.QPushButton(ImportScreen)
        self.bu_import_file.setMinimumSize(QtCore.QSize(150, 0))
        self.bu_import_file.setObjectName(_fromUtf8("bu_import_file"))
        self.horizontalLayout_2.addWidget(self.bu_import_file)
        spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem1)
        self.verticalLayout.addLayout(self.horizontalLayout_2)

        self.retranslateUi(ImportScreen)
        QtCore.QMetaObject.connectSlotsByName(ImportScreen)

    def retranslateUi(self, ImportScreen):
        ImportScreen.setWindowTitle(_translate("ImportScreen", "Importation de Données", None))
        self.lineEdit.setPlaceholderText(_translate("ImportScreen", "chemin du fichier", None))
        self.bu_choose_file.setText(_translate("ImportScreen", "Choisir fichier", None))
        self.groupBox.setTitle(_translate("ImportScreen", "Mode d\'import", None))
        self.rd_echeancier.setText(_translate("ImportScreen", "Echeancier", None))
        self.rd_titre.setText(_translate("ImportScreen", "Titres", None))
        self.rd_all_file.setText(_translate("ImportScreen", "Tout le fichier", None))
        self.rd_courbe_front.setText(_translate("ImportScreen", "Courbe BAM", None))
        self.rd_courbe.setText(_translate("ImportScreen", "Courbe", None))
        self.bu_import_file.setText(_translate("ImportScreen", "Importer", None))
