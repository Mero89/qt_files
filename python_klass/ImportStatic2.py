# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '/Users/mero/Projects/PricerMO/DPricer/ui/qtproject/TitreForm/import.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8


    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)


class Ui_ImportScreen(object):
    def setupUi(self, ImportScreen):
        ImportScreen.setObjectName(_fromUtf8("ImportScreen"))
        ImportScreen.resize(396, 180)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Fixed, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(ImportScreen.sizePolicy().hasHeightForWidth())
        ImportScreen.setSizePolicy(sizePolicy)
        ImportScreen.setMinimumSize(QtCore.QSize(360, 180))
        ImportScreen.setMaximumSize(QtCore.QSize(450, 230))
        self.verticalLayout = QtGui.QVBoxLayout(ImportScreen)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.horizontalLayout_3 = QtGui.QHBoxLayout()
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.label = QtGui.QLabel(ImportScreen)
        self.label.setObjectName(_fromUtf8("label"))
        self.horizontalLayout_3.addWidget(self.label)
        self.cb_choices = QtGui.QComboBox(ImportScreen)
        self.cb_choices.setObjectName(_fromUtf8("cb_choices"))
        self.cb_choices.addItem(_fromUtf8(""))
        self.cb_choices.addItem(_fromUtf8(""))
        self.cb_choices.addItem(_fromUtf8(""))
        self.cb_choices.addItem(_fromUtf8(""))
        self.cb_choices.addItem(_fromUtf8(""))
        self.cb_choices.addItem(_fromUtf8(""))
        self.cb_choices.addItem(_fromUtf8(""))
        self.horizontalLayout_3.addWidget(self.cb_choices)
        self.verticalLayout.addLayout(self.horizontalLayout_3)
        self.line = QtGui.QFrame(ImportScreen)
        self.line.setFrameShape(QtGui.QFrame.HLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName(_fromUtf8("line"))
        self.verticalLayout.addWidget(self.line)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setSizeConstraint(QtGui.QLayout.SetDefaultConstraint)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.lineEdit = QtGui.QLineEdit(ImportScreen)
        self.lineEdit.setMinimumSize(QtCore.QSize(160, 0))
        self.lineEdit.setInputMask(_fromUtf8(""))
        self.lineEdit.setObjectName(_fromUtf8("lineEdit"))
        self.horizontalLayout.addWidget(self.lineEdit)
        self.bu_choose_file = QtGui.QPushButton(ImportScreen)
        self.bu_choose_file.setMaximumSize(QtCore.QSize(110, 16777215))
        self.bu_choose_file.setObjectName(_fromUtf8("bu_choose_file"))
        self.horizontalLayout.addWidget(self.bu_choose_file)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.line_2 = QtGui.QFrame(ImportScreen)
        self.line_2.setFrameShape(QtGui.QFrame.HLine)
        self.line_2.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_2.setObjectName(_fromUtf8("line_2"))
        self.verticalLayout.addWidget(self.line_2)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem)
        self.bu_import_file = QtGui.QPushButton(ImportScreen)
        self.bu_import_file.setMinimumSize(QtCore.QSize(150, 0))
        self.bu_import_file.setObjectName(_fromUtf8("bu_import_file"))
        self.horizontalLayout_2.addWidget(self.bu_import_file)
        spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem1)
        self.verticalLayout.addLayout(self.horizontalLayout_2)

        self.retranslateUi(ImportScreen)
        QtCore.QMetaObject.connectSlotsByName(ImportScreen)

    def retranslateUi(self, ImportScreen):
        ImportScreen.setWindowTitle(_translate("ImportScreen", "Importation de Données", None))
        self.label.setText(_translate("ImportScreen", "Choisir les données à importer :", None))
        self.cb_choices.setItemText(0, _translate("ImportScreen", "MBI", None))
        self.cb_choices.setItemText(1, _translate("ImportScreen", "MBI CT", None))
        self.cb_choices.setItemText(2, _translate("ImportScreen", "MBI MT", None))
        self.cb_choices.setItemText(3, _translate("ImportScreen", "MBI MLT", None))
        self.cb_choices.setItemText(4, _translate("ImportScreen", "MBI LT", None))
        self.cb_choices.setItemText(5, _translate("ImportScreen", "MASI - MADEX", None))
        self.cb_choices.setItemText(6, _translate("ImportScreen", "TMP", None))
        self.lineEdit.setPlaceholderText(_translate("ImportScreen", "chemin du fichier", None))
        self.bu_choose_file.setText(_translate("ImportScreen", "Choisir fichier", None))
        self.bu_import_file.setText(_translate("ImportScreen", "Importer", None))
