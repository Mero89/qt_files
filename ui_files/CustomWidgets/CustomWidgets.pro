#-------------------------------------------------
#
# Project created by QtCreator 2016-06-15T22:38:51
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CustomWidgets
TEMPLATE = app


SOURCES +=

HEADERS  +=

FORMS += \
    benchmark.ui \
    calendar_widget.ui \
    dat.ui \
    database.ui \
    date_widget.ui \
    export.ui \
    import.ui \
    market_import.ui \
    mo.ui \
    rapports.ui \
    testmodels.ui \
    valo.ui \
    reporting_skope.ui \
    whiteboard.ui \
    yield.ui \
    pricer.ui \
    pricer_consult.ui \
    skope_main.ui \
    dpricer_main.ui \
    rapports_dpricer.ui \
    data_deleter.ui \
    wizard_excel.ui
