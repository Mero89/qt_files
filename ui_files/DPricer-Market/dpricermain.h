#ifndef DPRICERMAIN_H
#define DPRICERMAIN_H

#include <QMainWindow>

namespace Ui {
class DPricerMain;
}

class DPricerMain : public QMainWindow
{
    Q_OBJECT

public:
    explicit DPricerMain(QWidget *parent = 0);
    ~DPricerMain();

private:
    Ui::DPricerMain *ui;
};

#endif // DPRICERMAIN_H
