#-------------------------------------------------
#
# Project created by QtCreator 2016-07-11T00:36:43
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = DPricer-Market
TEMPLATE = app


SOURCES += main.cpp\
        dpricermain.cpp

HEADERS  += dpricermain.h

FORMS    += dpricermain.ui
