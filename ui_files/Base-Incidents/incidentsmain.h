#ifndef INCIDENTSMAIN_H
#define INCIDENTSMAIN_H

#include <QMainWindow>

namespace Ui {
class IncidentsMain;
}

class IncidentsMain : public QMainWindow
{
    Q_OBJECT

public:
    explicit IncidentsMain(QWidget *parent = 0);
    ~IncidentsMain();

private:
    Ui::IncidentsMain *ui;
};

#endif // INCIDENTSMAIN_H
