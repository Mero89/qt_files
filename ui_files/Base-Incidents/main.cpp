#include "incidentsmain.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    IncidentsMain w;
    w.show();

    return a.exec();
}
