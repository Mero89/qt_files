#include "incidentsmain.h"
#include "ui_incidentsmain.h"

IncidentsMain::IncidentsMain(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::IncidentsMain)
{
    ui->setupUi(this);
}

IncidentsMain::~IncidentsMain()
{
    delete ui;
}
