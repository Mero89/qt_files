#-------------------------------------------------
#
# Project created by QtCreator 2016-09-21T22:52:27
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Base-Incidents
TEMPLATE = app


SOURCES += main.cpp\
        incidentsmain.cpp

HEADERS  += incidentsmain.h

FORMS    += incidentsmain.ui
