#-------------------------------------------------
#
# Project created by QtCreator 2016-07-11T20:27:07
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Skope-UI
TEMPLATE = app


SOURCES +=

HEADERS  +=

FORMS    += \
    benchmark.ui \
    export.ui \
    reporting_skope.ui \
    skopemain.ui \
    safe_deleter.ui \
    labeled_import.ui \
    automata.ui \
    bdc_dat.ui \
    database.ui
